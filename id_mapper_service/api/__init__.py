from .v1 import add_routes_v1


def add_routes(app, late_init, api_v1_integration):
    add_routes_v1(app, late_init, api_v1_integration)
