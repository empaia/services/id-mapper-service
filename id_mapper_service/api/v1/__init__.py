from .cases import add_routes_cases
from .server import add_routes_misc
from .slides import add_routes_slides


def add_routes_v1(app, late_init, api_v1_integration):
    add_routes_cases(app, late_init, api_v1_integration)
    add_routes_slides(app, late_init, api_v1_integration)
    add_routes_misc(app, late_init, api_v1_integration)
