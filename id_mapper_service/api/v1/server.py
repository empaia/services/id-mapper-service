from ...db import db_clients
from ...models.commons import ServiceStatus


def add_routes_misc(app, late_init, api_v1_integration):
    @app.get(
        "/alive",
        tags=["Service"],
        summary="Returns service health status",
        responses={
            200: {"model": ServiceStatus},
        },
    )
    async def _():
        """
        Returns the health status of the service.

        Checks if database is running and responding.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.server.check_server_health()
