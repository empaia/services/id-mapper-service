from importlib.metadata import version

__version__ = version("id-mapper-service")
