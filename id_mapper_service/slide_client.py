from asyncpg.exceptions import UniqueViolationError

from .models.v1.id_mapper import Mapping, Mappings
from .singletons import logger


class SlideClient:
    def __init__(self, conn):
        self.conn = conn

    @staticmethod
    def _sql_mapping_as_json():
        return """json_build_object(
            'empaia_id', empaia_id,
            'mds_url', mds_url,
            'local_id', local_id
        )"""

    async def add(self, mapping: Mapping) -> Mapping:
        sql = f"""
        INSERT INTO wsis (empaia_id, mds_url, local_id)
        VALUES ($1, $2, $3)
        RETURNING {SlideClient._sql_mapping_as_json()};
        """
        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, str(mapping.empaia_id), str(mapping.mds_url), mapping.local_id)
        except UniqueViolationError as e:
            logger.debug(e)
            return

        if row is None:
            return

        return Mapping.model_validate(row["json_build_object"])

    async def get_local(self, empaia_id: str) -> Mapping:
        sql = f"""
        SELECT {SlideClient._sql_mapping_as_json()}
        FROM wsis
        WHERE empaia_id=$1;
        """
        logger.debug(sql)

        row = await self.conn.fetchrow(sql, str(empaia_id))

        if row is None:
            return

        return Mapping.model_validate(row["json_build_object"])

    async def get_locals(self, empaia_ids: list) -> Mappings:
        where_clause = None
        if empaia_ids is not None and len(empaia_ids) > 0:
            where_clause = "WHERE empaia_id = ANY($1)"

        sql = f"""
        SELECT {SlideClient._sql_mapping_as_json()}
        FROM wsis
        {where_clause};
        """
        logger.debug(sql)

        if where_clause is None:
            rows = await self.conn.fetch(sql)
        else:
            empaia_ids_str = [str(id) for id in empaia_ids]
            rows = await self.conn.fetch(sql, empaia_ids_str)

        if rows is None:
            return

        items = []
        for row in rows:
            items.append(Mapping.model_validate(row["json_build_object"]))

        return Mappings(items=items)

    async def delete(self, empaia_id: str):
        sql = """
        DELETE
        FROM wsis
        WHERE empaia_id=$1;
        """
        logger.debug(sql)

        await self.conn.execute(sql, str(empaia_id))

    async def get_empaia(self, local_id: str) -> Mappings:
        sql = f"""
        SELECT json_agg({SlideClient._sql_mapping_as_json()})
        FROM wsis
        WHERE local_id=$1;
        """
        logger.debug(sql)

        row = await self.conn.fetchrow(sql, local_id)

        if row is None:
            return

        items = None
        if row["json_agg"] is not None:
            items = row["json_agg"]

        if not items:
            return

        return Mappings(items=items)
