from typing import Set

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    db_host: str = "localhost"
    db_port: int = 5432
    db_username: str = "admin"
    db_password: str = "secret"
    db: str = "idm"
    cors_allow_credentials: bool = False
    cors_allow_origins: Set[str] = []
    debug: bool = False
    api_v1_integration: str = ""
    root_path: str = ""

    model_config = SettingsConfigDict(env_file=".env", env_prefix="idm_", extra="ignore")
