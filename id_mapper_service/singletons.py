import logging

from .settings import Settings

settings = Settings()

logger = logging.getLogger("uvicorn.idm")

if settings.debug:
    logger.setLevel(logging.DEBUG)
