import json

from asyncpg import Connection

from .case_client import CaseClient
from .server_client import ServerClient
from .slide_client import SlideClient


def _jsonb_dumps(value):
    return b"\x01" + json.dumps(value).encode("utf-8")


def _jsonb_loads(value: bytes):
    return json.loads(value[1:].decode("utf-8"))


async def set_type_codecs(conn: Connection):
    await conn.set_type_codec("json", encoder=json.dumps, decoder=json.loads, schema="pg_catalog", format="text")
    await conn.set_type_codec("jsonb", encoder=_jsonb_dumps, decoder=_jsonb_loads, schema="pg_catalog", format="binary")


class Clients:
    def __init__(self, conn):
        self.case = CaseClient(conn=conn)
        self.slide = SlideClient(conn=conn)
        self.server = ServerClient(conn=conn)


async def db_clients(conn: Connection):
    await set_type_codecs(conn=conn)
    return Clients(conn=conn)
