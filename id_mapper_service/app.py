from contextlib import asynccontextmanager

import asyncpg
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from . import __version__ as version
from .api import add_routes
from .api_integrations import get_api_v1_integration
from .late_init import LateInit
from .singletons import logger, settings

late_init = LateInit()


@asynccontextmanager
async def lifespan(fastapi_app: FastAPI):
    late_init.pool = await asyncpg.create_pool(
        user=settings.db_username,
        password=settings.db_password,
        database=settings.db,
        host=settings.db_host,
        port=settings.db_port,
    )
    yield


app = FastAPI(
    debug=settings.debug,
    title="ID Mapper Service",
    version=version,
    description="ID Mapper Service",
    root_path=settings.root_path,
    lifespan=lifespan,
)


if settings.cors_allow_origins:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=settings.cors_allow_origins,
        allow_credentials=settings.cors_allow_credentials,
        allow_methods=["*"],
        allow_headers=["*"],
    )

api_v1_integration = get_api_v1_integration(settings=settings, logger=logger)
add_routes(app, late_init, api_v1_integration)
