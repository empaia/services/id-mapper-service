from asyncpg.exceptions import PostgresError
from fastapi.exceptions import HTTPException

from . import __version__ as version
from .models.commons import ServiceStatus


class ServerClient:
    def __init__(self, conn):
        self.conn = conn

    async def check_server_health(self) -> ServiceStatus:
        # currently only database connection is checked
        try:
            r = await self.conn.fetch("SELECT datname FROM pg_database;")
            if r is not None:
                return ServiceStatus(status="ok", version=version)
            else:
                raise HTTPException(200, {"status": "ERROR: Database Server not responding!"})
        except PostgresError as e:
            raise HTTPException(200, {"status": "ERROR: Database Server not responding!"}) from e
