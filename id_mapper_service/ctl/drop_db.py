from asyncpg.exceptions import UndefinedTableError

from .commons import connect_db

TABLES = [
    "migration_steps",
    "cases",
    "wsis",
]


async def run_drop_db():
    conn = await connect_db()

    for table in TABLES:
        try:
            await conn.execute(f"DROP TABLE {table};")
        except UndefinedTableError as e:
            print(e)
