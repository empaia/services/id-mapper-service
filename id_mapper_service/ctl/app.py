import asyncio

import typer

from .drop_db import run_drop_db
from .migrate_db import run_migrate_db

app = typer.Typer()


@app.command()
def migrate_db():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run_migrate_db())


@app.command()
def drop_db():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run_drop_db())
