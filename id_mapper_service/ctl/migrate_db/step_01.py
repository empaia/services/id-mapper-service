async def step_01(conn):
    sql = """
    CREATE TABLE cases (
        empaia_id text PRIMARY KEY,
        mds_url text NOT NULL,
        local_id text NOT NULL
    );
    """
    await conn.execute(sql)

    sql = """
    CREATE TABLE wsis (
        empaia_id text PRIMARY KEY,
        mds_url text NOT NULL,
        local_id text NOT NULL
    );
    """
    await conn.execute(sql)
