import asyncio

from asyncpg.exceptions import CannotConnectNowError

from ..commons import connect_db
from .step_01 import step_01
from .step_02 import step_02

MIGRATION_STEPS = [step_01, step_02]


async def _run_migrate_db(target_step=None):
    conn = await connect_db()

    async with conn.transaction():
        step_id = 0

        row = await conn.fetchrow(
            """
            SELECT EXISTS (
                SELECT FROM information_schema.tables
                WHERE table_schema = 'public' AND table_name = 'migration_steps'
            );
            """
        )
        if not row["exists"]:
            await conn.execute(
                """
                CREATE TABLE migration_steps (
                    id SERIAL PRIMARY KEY,
                    timestamp TIMESTAMP DEFAULT current_timestamp
                );
                """
            )
        else:
            row = await conn.fetchrow(
                """
                SELECT id FROM migration_steps ORDER BY id DESC
                """
            )

            if row is not None:
                step_id = row["id"]

        remaining_steps = MIGRATION_STEPS[step_id:]
        if target_step is not None:
            remaining_steps = MIGRATION_STEPS[step_id:target_step]

        for i, step in enumerate(remaining_steps):
            print(f"DATABASE MIGRATION STEP {step_id + i + 1}")
            await step(conn=conn)
            await conn.execute("INSERT INTO migration_steps DEFAULT VALUES returning id;")


async def run_migrate_db(target_step=None, trial_interval=1, trials=30):
    print("START DATABASE MIGRATIONS")

    migration_completed = False

    for i in range(trials):
        try:
            await _run_migrate_db(target_step=target_step)
            migration_completed = True
            break
        except (ConnectionRefusedError, CannotConnectNowError):
            print(
                f"WARNING: ConnectionRefusedError during migration trial {i+1}/{trials}, "
                f"trying again in {trial_interval} seconds..."
            )

        await asyncio.sleep(trial_interval)

    if not migration_completed:
        raise Exception("ERROR: Could not connect to DB for migration")

    print("END DATABASE MIGRATIONS")
