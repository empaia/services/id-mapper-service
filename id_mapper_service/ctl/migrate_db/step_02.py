async def step_02(conn):
    sql = """
    CREATE INDEX cases_local_id ON cases USING btree (local_id);
    CREATE INDEX wsis_local_id ON wsis USING btree (local_id);
    """
    await conn.execute(sql)
