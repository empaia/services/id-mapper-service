# ID Mapper Service

## Dev Setup

* install docker
* install docker-compose
* install pyenv
* install Python 3.8 via pyenv
* install poetry
* clone id-mapper-service

Then run the following commands to create a new virtual environment and install project dependencies.

```bash
cd id-mapper-service
python3 -m venv .venv
source .venv/bin/activate
poetry install
```

### Start Service

Set environment variables in a `.env` file.

```
cp sample.env .env  # adapt as needed
# cp sample_auth.env .env  # enables receiver auth
docker-compose up --build -d idm
```

### Code Checks

Check your code before committing.

* always format code with `black` and `isort`
* check code quality using `pycodestyle` and `pylint`
    * `black` formatting should resolve most issues for `pycodestyle`
* run tests with `pytest`

```bash
isort .
black .
pycodestyle id_mapper_service tests tests_auth
pylint id_mapper_service tests tests_auth
pytest tests --maxfail=1
```

### Recommended VSCode Settings

In `.vscode/settings.json`.

```json
{
    "python.linting.pylintEnabled": true,
    "python.linting.pycodestyleEnabled": false,
    "python.formatting.provider": "black"
}
```

## Usage

Open `http://localhost:8000/docs` in your Browser.
