# 0.2.20

* remove redundant codecheck

# 0.2.19

* using FastAPI lifespan instead of startup event

# 0.2.18

* renovate

# 0.2.17

* renovate

# 0.2.15

* renovate

# 0.2.14

* renovate

# 0.2.13

* renovate

# 0.2.12

* renovate

# 0.2.11

* renovate

# 0.2.10

* renovate

# 0.2.9

* renovate

# 0.2.8

* renovate

# 0.2.7

* renovate

# 0.2.6

* renovate

# 0.2.5

* renovate

# 0.2.4

* renovate

# 0.2.3

* renovate

# 0.2.2

* renovate

# 0.2.1

* bugfix for empty `rewrite_url_in_wellknown` env variable

# 0.2.0

* migrate to pydantic v2

# 0.1.68

* renovate

# 0.1.67

* renovate

# 0.1.66

* renovate

# 0.1.65

* renovate

# 0.1.64

* renovate

# 0.1.63

* renovate

# 0.1.62

* renovate

# 0.1.61

* renovate

# 0.1.60

* renovate

# 0.1.59

* renovate

# 0.1.58

* renovate

# 0.1.57

* renovate

# 0.1.56

* renovate

# 0.1.55

* renovate

# 0.1.54

* renovate

# 0.1.53

* renovate

# 0.1.52

* renovate

# 0.1.51

* renovate

# 0.1.50

* renovate

# 0.1.49

* renovate

# 0.1.48

* renovate

# 0.1.47

* renovate

# 0.1.46

* renovate

# 0.1.45

* renovate

# 0.1.44

* renovate

# 0.1.43

* renovate

# 0.1.42

* renovate

# 0.1.41

* renovate

# 0.1.40

* renovate

# 0.1.39

* renovate

# 0.1.38

* renovate

# 0.1.37

* renovate

# 0.1.36

* renovate

# 0.1.35

* renovate

# 0.1.34

* renovate

# 0.1.33

* renovate

# 0.1.32

* renovate

# 0.1.31

* renovate

# 0.1.30

* renovate

# 0.1.29

* renovate

# 0.1.28

* renovate

# 0.1.27

* added query endpoints for cases and slides

# 0.1.26

* renovate

# 0.1.25

* renovate

# 0.1.24

* renovate

# 0.1.23

* renovate

# 0.1.22

* renovate

# 0.1.21

* renovate

# 0.1.20

* renovate

# 0.1.19

* renovate

# 0.1.18

* updated dependencies

# 0.1.17

* updated ci

# 0.1.16

* added cors_allow_origins to settings again and made allow_credentials also configurable via cors_allow_credentials

# 0.1.15

* allow all origins, which is now possible because frontends do no more use client credentials (instead they explicitly 
use an authorization header)

# 0.1.14

* updated receiver-auth
* added setting to rewrite URL in wellknown

# 0.1.13

* updated tests to use local keycloak

# 0.1.12

* update empaia-receiver-auth
* added new env var IDM_OPENAPI_TOKEN_URL

# 0.1.11

* configurable token URL path

# 0.1.10

* added endpoints to delete mappings for cases and slides

# 0.1.9

* /alive endpoint
* added models repo as submodule
* outsources models to models repo

# 0.1.8

* updated db-migration

# 0.1.7

* removed models submodule and added submodules directly

# 0.1.6

* updated auth settings and CI

# 0.1.5

* added ROOT_PATH setting

# 0.1.3

* read version from package

# 0.1.2

* pinned PostgreSQL version in Docker compose

# 0.1.0

* added switch to enable auth for tests
  * added dependency
* added empaia-receiver-auth
  * .api.v1.intetrations.empaia.py
  * added dependencies pyjwt, cryptography, requests
  * added dp Engine arg to diable_auth and default integrations
  * added env vars to readme to enable auth
