#!/usr/bin/env bash

idmctl migrate-db && uvicorn id_mapper_service.app:app $@
