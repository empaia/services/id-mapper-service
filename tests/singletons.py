from .login_manager import LoginManager
from .settings import Settings

settings = Settings()
idm_url = settings.idm_url.rstrip("/")
login_manager = LoginManager()
