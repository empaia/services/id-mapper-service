from uuid import uuid4

import requests

from id_mapper_service.models.v1.id_mapper import Mapping, Mappings

from .singletons import idm_url, login_manager


def test_slides():
    empaia_id = str(uuid4())
    local_id = str(uuid4())
    headers = login_manager.mta_user()
    post_slide(empaia_id, local_id, headers)
    get_slide_by_empaia_id(empaia_id, local_id, headers)
    get_slides_by_local_id(empaia_id, local_id, headers)
    get_slide_by_empaia_id_404(str(uuid4()), headers)
    get_slides_by_local_id_404(str(uuid4()), headers)
    delete_slide(empaia_id, headers)
    try:
        delete_slide(empaia_id, headers)
        assert False
    except requests.exceptions.HTTPError as e:
        assert e.response.status_code == 404
        assert "EMPAIA ID not found" in e.response.text
    post_slide(empaia_id, local_id, headers)
    try:
        post_slide(empaia_id, local_id, headers)
        assert False
    except requests.exceptions.HTTPError as e:
        assert e.response.status_code == 409
        assert "already exists" in e.response.text


def test_query_slides_mappings():
    headers = login_manager.mta_user()

    empaia_ids_dict = {}
    for _ in range(0, 10):
        empaia_id = str(uuid4())
        local_id = f"local-{str(uuid4())}"
        post_slide(empaia_id, local_id, headers)
        empaia_ids_dict[empaia_id] = local_id

    empaia_ids = []
    result = put_slide_empaia_ids(empaia_ids=empaia_ids, headers=headers)
    assert len(result["items"]) >= 10

    empaia_ids = list(empaia_ids_dict.keys())[0:5]
    result = put_slide_empaia_ids(empaia_ids=empaia_ids, headers=headers)
    assert len(result["items"]) == 5
    for empaia_id in empaia_ids:
        entry = next((x for x in result["items"] if x["empaia_id"] == empaia_id), None)
        assert entry is not None
        assert empaia_ids_dict[empaia_id] == entry["local_id"]

    empaia_ids = list(empaia_ids_dict.keys())
    result = put_slide_empaia_ids(empaia_ids=empaia_ids, headers=headers)
    assert len(result["items"]) == 10
    for empaia_id in empaia_ids:
        entry = next((x for x in result["items"] if x["empaia_id"] == empaia_id), None)
        assert entry is not None
        assert empaia_ids_dict[empaia_id] == entry["local_id"]


def post_slide(empaia_id, local_id, headers):
    slide = {"empaia_id": empaia_id, "mds_url": "https://mds.example.org", "local_id": local_id}
    r = requests.post(f"{idm_url}/v1/slides", json=slide, headers=headers)
    r.raise_for_status()
    data = r.json()
    Mapping.model_validate(data)


def get_slide_by_empaia_id(empaia_id, local_id, headers):
    r = requests.get(f"{idm_url}/v1/slides/empaia/{empaia_id}", headers=headers)
    r.raise_for_status()
    data = r.json()
    mapping = Mapping.model_validate(data)
    assert str(mapping.empaia_id) == empaia_id
    assert str(mapping.local_id) == local_id


def delete_slide(empaia_id, headers):
    r = requests.delete(f"{idm_url}/v1/slides/empaia/{empaia_id}", headers=headers)
    r.raise_for_status()
    data = r.json()
    assert f"deleted slide {empaia_id}" == data["message"]


def get_slides_by_local_id(empaia_id, local_id, headers):
    r = requests.get(f"{idm_url}/v1/slides/local/{local_id}", headers=headers)
    r.raise_for_status()
    data = r.json()
    mappings = Mappings.model_validate(data)
    assert len(mappings.items) == 1
    mapping = mappings.items[0]
    assert str(mapping.empaia_id) == empaia_id
    assert str(mapping.local_id) == local_id


def get_slide_by_empaia_id_404(empaia_id, headers):
    r = requests.get(f"{idm_url}/v1/slides/empaia/{empaia_id}", headers=headers)
    print(r.json())
    assert r.status_code == 404
    assert "ID not found" in r.json()["detail"]


def get_slides_by_local_id_404(local_id, headers):
    r = requests.get(f"{idm_url}/v1/slides/local/{local_id}", headers=headers)
    print(r.json())
    assert r.status_code == 404
    assert "ID not found" in r.json()["detail"]


def put_slide_empaia_ids(empaia_ids, headers):
    json = {"empaia_ids": empaia_ids}
    r = requests.put(f"{idm_url}/v1/slides/empaia/query", json=json, headers=headers)
    r.raise_for_status()
    return r.json()
