import requests

from .singletons import idm_url, login_manager


def test_alive():
    headers = login_manager.mta_user()
    r = requests.get(f"{idm_url}/alive", headers=headers)
    r.raise_for_status()
    data = r.json()
    print(data)
    assert data["status"] == "ok"
