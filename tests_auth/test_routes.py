import json

import requests

from .settings import Settings

settings = Settings()
idm_url = settings.idm_url.strip("/")


EXCLUDED_PATHS = ["/alive"]


def test_routes():
    r = requests.get(f"{idm_url}/openapi.json")
    openapi = json.loads(r.content)
    for path, ops in openapi["paths"].items():
        if path in EXCLUDED_PATHS:
            continue

        path_no_vars = path.replace("{", "").replace("}", "")
        url = f"{idm_url}{path_no_vars}"
        for op in ops.keys():
            print(op, url)
            r = requests.request(op, url)
            assert r.status_code == 403
