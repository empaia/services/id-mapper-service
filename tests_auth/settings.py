from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    idm_url: str

    model_config = SettingsConfigDict(env_file=".env", env_prefix="pytest_", extra="ignore")
